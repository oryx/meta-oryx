SUMMARY = "Oryx universal image recipe."
LICENSE = "MIT"

inherit core-image

IMAGE_BASENAME = "${ORYX_IMAGE_BASENAME}"

ORYX_CORE_PACKAGES = " \
    packagegroup-core-boot \
    openssh-sshd \
    openssh-sftp-server \
    openssh-scp \
    os-release \
    "

IMAGE_INSTALL = " \
    ${ORYX_CORE_PACKAGES} \
    ${ORYX_SYSTEM_PROFILE_PACKAGES} \
    ${ORYX_APPLICATION_PROFILE_PACKAGES} \
    "
IMAGE_LINGUAS = " "

# Set root password to 'oryx'
inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P oryx root;"

ORYX_CORE_OUTPUT_FILES = "image_${ORYX_SYSTEM_PROFILE_TYPE}.json"
ORYX_CORE_OUTPUT_DEPENDS = "image-json-file:do_build"
ORYX_SYSTEM_PROFILE_OUTPUT_FILES ??= ""
ORYX_SYSTEM_PROFILE_OUTPUT_DEPENDS ??= ""

ORYX_OUTPUT_STAGING_DIR = "${WORKDIR}/oryx-output"

python do_oryx_output() {
    import shutil

    dest_dir = d.getVar('ORYX_OUTPUT_STAGING_DIR')
    src_dir = d.getVar('DEPLOY_DIR_IMAGE')
    output_files = d.getVar('ORYX_CORE_OUTPUT_FILES').split() + d.getVar('ORYX_SYSTEM_PROFILE_OUTPUT_FILES').split()

    # Start from an empty directory to avoid mixing output from different builds
    if os.path.exists(dest_dir):
        shutil.rmtree(dest_dir)
    os.makedirs(dest_dir)

    for fname in output_files:
        src = os.path.join(src_dir, fname)
        dest = os.path.join(dest_dir, fname)
        shutil.copy(src, dest)
}

do_oryx_output[depends] = "${ORYX_CORE_OUTPUT_DEPENDS} ${ORYX_SYSTEM_PROFILE_OUTPUT_DEPENDS}"
addtask do_oryx_output after do_image_complete before do_build

SSTATETASKS += "do_oryx_output"
do_oryx_output[sstate-inputdirs] = "${ORYX_OUTPUT_STAGING_DIR}"
do_oryx_output[sstate-outputdirs] = "${ORYX_OUTPUT_DIR}/${MACHINE}/${ORYX_SYSTEM_PROFILE}/${ORYX_APPLICATION_PROFILE}"

python do_oryx_output_setscene() {
    sstate_setscene(d)
}
addtask do_oryx_output_setscene
do_oryx_output[stamp-extra-info] = "${MACHINE}_${ORYX_SYSTEM_PROFILE}_${ORYX_APPLICATION_PROFILE}"
