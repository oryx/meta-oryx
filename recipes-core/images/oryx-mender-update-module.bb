inherit allarch

SUMMARY = "Create Mender Update Module artifact for an Oryx image"
LICENSE = "MIT"
INHIBIT_DEFAULT_DEPS = "1"

do_fetch[noexec] = "1"
do_unpack[noexec] = "1"
do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_install[noexec] = "1"
do_package[noexec] = "1"
do_packagedata[noexec] = "1"
do_package_write_ipk[noexec] = "1"
do_package_write_rpm[noexec] = "1"
do_package_write_deb[noexec] = "1"
do_populate_sysroot[noexec] = "1"

DEPENDS += "mender-artifact-native"

do_compile() {
    mender-artifact write module-image \
        -t ${MENDER_DEVICE_TYPE} \
        -o update_module.mender \
        -T oryx-update \
        -n rootfs \
        -f "${DEPLOY_DIR_IMAGE}/${ORYX_ROOTFS_IMAGE}" \
        -f "${DEPLOY_DIR_IMAGE}/image_${ORYX_SYSTEM_PROFILE_TYPE}.json"
}

do_compile[depends] = "oryx-image:do_image_complete image-json-file:do_build"

inherit deploy

do_deploy() {
    install -d ${DEPLOYDIR}
    install -m 0644 update_module.mender ${DEPLOYDIR}
}

addtask do_deploy after do_compile before do_build
