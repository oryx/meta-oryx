DESCRIPTION = "Oryx Applications"
HOMEPAGE = "https://gitlab.com/oryx/oryx-apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=5411e38f262e8af379a1a1cc4e92134f"

inherit allarch systemd

PV = "0.3.0"

SRC_URI = "git://gitlab.com/oryx/oryx-apps.git;protocol=https"
SRCREV = "d344f51074051b11243e62a0ed6bb17736f08d7c"
S = "${WORKDIR}/git"

do_install() {
    oe_runmake install DESTDIR="${D}" PREFIX="${base_prefix}"
}

PACKAGES_prepend = "oryxcmd oryxcmd-test "

FILES_oryxcmd = " \
    ${base_sbindir}/oryxcmd \
    ${systemd_unitdir}/system/oryx-guests.service \
    "

# Really, we should rename this, but to get around sdk failure
FILES_oryxapps = " \
    ${base_sbindir}/oryxcmd \
    ${systemd_unitdir}/system/oryx-guests.service \
    " 

RDEPENDS_oryxcmd = " \
    python3-compression \
    python3-fcntl \
    python3-json \
    python3-logging \
    python3-netclient \
    python3-shell \
    "
FILES_oryxcmd-test = "${base_sbindir}/oryxcmd-test"

RDEPENDS_oryxcmd-test = " \
    oryxcmd \
    python3-unittest \
    python3-betatest \
    "

SYSTEMD_PACKAGES = "oryxcmd"
SYSTEMD_SERVICE_oryxcmd = "oryx-guests.service"

ALLOW_EMPTY_${PN} = "1"
