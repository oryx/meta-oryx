#!/bin/sh

set -e

STATE="$1"
FILES="$2"

if [ "$#" -ne 2 ]; then
    echo "Invalid number of parameters"
    exit 1
fi

LOCAL_SOURCE="local"
IMAGE_JSON="$FILES""/files/image.json"

case "$STATE" in

    NeedsArtifactReboot)
        echo "No"
    ;;

    SupportsRollback)
        echo "No"
    ;;

    ArtifactInstall)
        # Check that the local source is configured and get the image dir
        [ $(oryxcmd list_sources) == "$LOCAL_SOURCE" ]
        ORYX_IMAGES=$(oryxcmd show_source "$LOCAL_SOURCE" | grep "url" | cut -d"\"" -f4 | cut -c 8-)"/guest"

        # Get the name of the current guest - assumes only one
        GUEST=$(oryxcmd list_guests)
        [ ! -z "$GUEST" ]
        IMAGE=$(ls "$ORYX_IMAGES"/"$GUEST"/*.tar.xz)

        # Check if the guest is running and stop it
        RUNNING=$(runc state $GUEST | grep "\"status\": \"running\"")
        if [ -z "$RUNNING" ]
        then
            $(oryxcmd stop_guest $GUEST)
        fi

        # Move the new guest image to the local source dir
        IMAGE_VERSION=$(sed -n '/VERSION/p' "$IMAGE_JSON" | cut -d'"' -f4)
        IMAGE_FILE=$(ls "$FILES"/files/oryx-guest-*.tar.xz)
        GUEST_NAME=$(echo "$IMAGE_FILE" | xargs -n 1 basename | cut -d"." -f1)_"$IMAGE_VERSION"
        mkdir -p "$ORYX_IMAGES"/"$GUEST_NAME"
        cp "$IMAGE_JSON" "$IMAGE_FILE" "$ORYX_IMAGES"/"$GUEST_NAME"

        # Add and start the new guest
        oryxcmd add_guest "$GUEST_NAME" "$LOCAL_SOURCE":"$GUEST_NAME"
        oryxcmd start_guest "$GUEST_NAME"

        # Remove the old guest and delete the files
        oryxcmd remove_guest "$GUEST"
        OLD_PATH=$(dirname "$IMAGE")
        rm -r "$OLD_PATH"/*.json "$OLD_PATH"/*.tar.xz "$OLD_PATH"
    ;;

    ArtifactRollback)
        # Remove new guest from oryxcmd
        IMAGE_VERSION=$(sed -n '/VERSION/p' "$IMAGE_JSON" | cut -d'"' -f4)
        IMAGE_FILE=$(ls "$FILES"/files/oryx-guest-*.tar.xz)
        GUEST_NAME=$(echo "$IMAGE_FILE" | xargs -n 1 basename | cut -d"." -f1)_"$IMAGE_VERSION"
        oryxcmd remove_guest "$GUEST_NAME"

        # Remove new files and directory
        [ $(oryxcmd list_sources) == "$LOCAL_SOURCE" ]
        ORYX_IMAGES=$(oryxcmd show_source "$LOCAL_SOURCE" | grep "url" | cut -d"\"" -f4)"/guest"
        SOURCE_PATH="$ORYX_IMAGES"/"$GUEST_NAME"
        rm -r "$SOURCE_PATH"/*.json "$SOURCE_PATH"/*.tar.xz "$SOURCE_PATH"

        # Try to start the old guest
        GUEST=$(oryxcmd list_guests)
        oryxcmd start_guest "$GUEST"
    ;;

esac
