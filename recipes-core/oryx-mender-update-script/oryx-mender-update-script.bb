inherit allarch

SUMMARY = "Oryx guest update handler for Mender update modules"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "file://oryx-update"

RDEPENDS_${PN} += "oryx-local-feed"

do_install() {
   install -d "${D}/usr/share/mender/modules/v3"
   install -m 755 "${WORKDIR}/oryx-update" "${D}/usr/share/mender/modules/v3"
}
