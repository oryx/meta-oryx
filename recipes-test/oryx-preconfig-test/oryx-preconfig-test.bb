SUMMARY = "Test recipe for Oryx preconfigured guest support"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

inherit allarch

SRC_URI = "file://10-preconfig-test.conf"

do_install() {
    install -d -m 0755 "${D}${datadir}/oryx/preconfig.d"
    install -m 0644 "${WORKDIR}/10-preconfig-test.conf" "${D}${datadir}/oryx/preconfig.d"
}

FILES_${PN} = "${datadir}/oryx"
