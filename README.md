meta-oryx
=========

This is the Oryx distro metadata layer for use with OpenEmbedded/Yocto Project.
This layer is maintained by Togán Labs.

This layer is just one component of the Oryx distro. For details on how to
checkout a full build tree and build images see the top-level README file at
https://gitlab.com/oryx/oryx/blob/master/README.md.

For support requests, bug reports or other feedback please open an issue in the
Oryx issue tracker (https://gitlab.com/oryx/oryx/issues) or contact us at
support@toganlabs.com.

To submit patches, please open a merge request on our GitLab repository
(https://gitlab.com/oryx/meta-oryx).
